#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

//written by Amy Dahlberg

main()
{
int s1 = socket(AF_INET,SOCK_STREAM,0); //making a socket
int status;
struct sockaddr_in server; //server settings
server.sin_family = AF_INET;
server.sin_port = htons(49153);
inet_pton(AF_INET,"10.115.20.250",&server.sin_addr); //IP address, connecting
status = connect(s1,(const struct sockaddr*)&server,sizeof(server)); //connect to server

char *username;
char chat[80]; //storing the input
char *quit = "q\n";
char buffer[80] = {0}; //buffer to receive messages

printf("username:  ");
fgets(username,80,stdin);
char user[80];
strcpy(user,username); //formatting the username
user[strlen(user)-1] = 0; //taking off newline character
strcat(user,":  ");
send(s1,username,strlen(username),0); //username to server
sleep(1); //delaying
printf(user);
fgets(chat,80,stdin); //storing input in chat
//printf("chat: %s\n",chat); //debugging

while (strcmp(chat,quit) != 0){ //go through loop if chat isn't q
  if (chat != "\n"){ //if there is input
    send(s1,chat,strlen(chat),0); //send the input to server
    memset(chat,0,strlen(chat)); //clear where the input is stored
    sleep(1); //delaying
    printf(user);
    fgets(chat,80,stdin); //getting next input
  }
  //printf("working\n"); //debugging
  sleep(1);
  recv(s1,buffer,80,0); //receiving information from server
  printf("%s\n",buffer); //prints information from other clients
  memset(buffer,0,sizeof(buffer)); //clears out buffer
  //printf("buffer: %s",buffer); //debugging
  fgets(chat,80,stdin);
}
//printf("close"); //debugging
close(s1); //close socket
}
